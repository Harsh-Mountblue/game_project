const gameContainer = document.getElementById("game");

let COLORS = [];
let cardCount = 4;

function randomColor() {
    return Math.floor(Math.random() * 16777215).toString(16);
}

function colorArr() {
    if (cardCount < 1) {
        cardCount = 1;
    }
    for (let index = 0; index < Math.ceil(cardCount / 2); index++) {
        let color = randomColor();
        // while (color[0] === 'e' || color[0] === 'f' || color[0] === 'E' || color[0] === 'F') {
        // color = randomColor();
        // }
        while (color.length < 6) {
          color = '0'+color;
        }
        COLORS.push(color);
        COLORS.push(color);
    }
    // console.log(COLORS);
}
// colorArr();
// COLORS = [
//     "red",
//     "blue",
//     "green",
//     "orange",
//     "purple",
//     "red",
//     "blue",
//     "green",
//     "orange",
//     "purple",
//     "cyan",
//     "cyan"
// ];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    let count = 1;
    // let length = colorArray.length;
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        let row = Math.ceil(Math.sqrt(cardCount));
        newDiv.style.width = `${60 / (row)}%`;
        newDiv.style.height = `${50 / row}%`;

        // newDiv.setAttribute('class','box')
        // give it a class attribute for the value we are looping over
        newDiv.classList.add(color);

        newDiv.setAttribute("id", count);
        // document.getElementById(count).style.width = `${80/row}%`
        count += 1;
        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}

// TODO: Implement this function!
function handleCardClick(event) {
    // you can use event.target to see which element was clicked
    // console.log("you clicked", event.target);

    // console.log(event.target);
    // document.getElementById(event.target.id).style.backgroundColor =
    //     event.target.className;
    divClick(event.target.id, event.target.className, shuffledColors.length);

    // console.log(this.color);
}

function select() {
    let firstClass = null;
    let firstId = null;
    let secondClass = null;
    let secondId = null;
    let guessCount = 0;
    let matchCount = 0;
    let minimumGuess = localStorage.getItem(`highScore${cardCount}`);
    // if (minimumGuess >= shuffledColors.length || minimumGuess!=null) {
    //     document.getElementById(
    //         "score"
    //     ).innerHTML = `Best Score: ${minimumGuess}`;
    // } else {
    //     document.getElementById("score").innerHTML = `Best Score: -`;
    // }
    return function selectDiv(divId, divClass, length) {
        // console.log(`highScore${cardCount}`);
        // if (minimumGuess) {
        // document.getElementById("score").innerHTML = `Best Score: ${minimumGuess}`;
        // } else {
        //     document.getElementById("score").innerHTML = `Best Score: -`;
        // }
        reset.addEventListener("click", () => {
            document.getElementById("result").innerHTML = "";
            document.getElementById("game").innerHTML = "";
            document.getElementById("guess").innerHTML = `Guesses: 0`;
            guessCount = 0;
            matchCount = 0;
            createDivsForColors(shuffle(COLORS));
        });
        if (divClass != "matched") {
            if (!firstClass) {
                firstClass = divClass;
                firstId = divId;
                document.getElementById(
                    divId
                ).style.background = `#${divClass}`;
                guessCount += 1;
            } else if (!secondClass && firstId != divId) {
                secondClass = divClass;
                secondId = divId;
                document.getElementById(
                    divId
                ).style.background = `#${divClass}`;
                guessCount += 1;
            }
            document.getElementById(
                "guess"
            ).innerHTML = `Guesses: ${guessCount}`;

            if (firstClass === secondClass) {
                document
                    .getElementById(firstId)
                    .setAttribute("class", "matched");
                document
                    .getElementById(secondId)
                    .setAttribute("class", "matched");
                firstClass = null;
                firstId = null;
                secondClass = null;
                secondId = null;
                matchCount += 2;

                if (matchCount === length) {
                    if (minimumGuess <= 0 || guessCount < minimumGuess) {
                        minimumGuess = guessCount;
                        localStorage.setItem(
                            `highScore${cardCount}`,
                            minimumGuess
                        );
                        // console.log("new best score", minimumGuess);
                        // guessCount = 0;
                        // matchCount = 0;
                        document.getElementById(
                            "score"
                        ).innerHTML = `Best Score: ${minimumGuess}`;
                    }
                    document.getElementById(
                        "result"
                    ).innerHTML = `Game completed in ${guessCount} guesses <button id="restart"> Restart </button>`;
                    restart.addEventListener("click", () => {
                        location.reload();
                        // document.getElementById("result").innerHTML = "";
                        // document.getElementById("game").innerHTML = "";
                        // document.getElementById(
                        //     "guess"
                        // ).innerHTML = `Guesses: 0`;
                        // guessCount = 0;
                        // matchCount = 0;
                        // COLORS = [];
                        // colorArr();
                        // createDivsForColors(shuffle(COLORS));
                    });
                    // console.log("game completed");
                }
            } else {
                setTimeout(() => {
                    if (firstId && secondId) {
                        document.getElementById(firstId).style.background =
                            "url(https://img.freepik.com/free-vector/abstract-organic-lines-background_1017-26669.jpg?size=626&ext=jpg)";
                        document.getElementById(secondId).style.background =
                            "url(https://img.freepik.com/free-vector/abstract-organic-lines-background_1017-26669.jpg?size=626&ext=jpg)";
                        secondId = null;
                        firstId = null;
                        firstClass = null;
                        secondClass = null;
                    }
                }, 1000);
            }

            // if (secondClass) {
            //     firstClass = null;
            //     secondClass = null;
            // }
        }
    };
}
let divClick = select();

// when the DOM loads
startGame.addEventListener("click", () => {
    // cardCount = document.querySelector("input").value;

    // cardCount.
    // let row = Math.ceil(Math.sqrt(cardCount));
    colorArr();
    shuffledColors = shuffle(COLORS);
    // document.querySelectorAll('div').style.height = `${80/row}%`;
    // console.log(cardCount);

    // document.getElementById("score").innerHTML = `Best Score: `;
    document.getElementById("game").innerHTML = "";
    document.getElementById("startGame").style.display = "none";
    document.getElementById("input").style.display = "none";

    let bestScore = localStorage.getItem(`highScore${cardCount}`);
    if (bestScore) {
        document.getElementById("score").innerHTML = `Best Score: ${bestScore}`;
    } else {
        document.getElementById("score").innerHTML = `Best Score: --`;
    }
    document.getElementById("game").style.display = "flex";
    document.getElementById("game").style.flexWrap = "wrap";
    document.getElementById("reset").style.display = "block";
    document.getElementById("result").innerHTML = "";
    document.getElementById("guess").innerHTML = `Guesses: 0`;
    createDivsForColors(shuffledColors);
});

// let box = document.querySelectorAll('.box');
// console.log(box)

// box[0].addEventListener('click', () => {
//   console.log(2);
//   console.log(box[0].id);
//   document.getElementById(box[0].id).style.backgroundColor = "red";
//   // box[0].style.backgroundColor = "red";
//   // console.log(1);
// })

function level() {
    let list = document.getElementById("input");
    let option = list.options[list.selectedIndex].text;
    // cardCount =  option.options[option.selectedIndex].text;
    // console.log(option);
    switch (option) {
        case "New Born Panda":
            cardCount = 4;
            break;
        case "Baby Panda":
            cardCount = 12;
            break;
        case "Teenage Panda":
            cardCount = 20;
            break;
        case "Adult Panda":
            cardCount = 30;
            break;
        case "Expert Panda":
            cardCount = 40;
            break;
    }
    // console.log(cardCount);

    // document.getElementById("favourite").value = cardCount.options[cardCount.selectedIndex].text;
}

const dropDown = document.getElementById("input");

// function options() {
//   for (index = 2; index < 55 ; index += 2) {
//     const option = document.createElement("option");
//     option.innerHTML = `${index}`;
//     option.setAttribute('id',index);
//     dropDown.append(option);
//     // console.log(option)
//   }
// }
// options();
